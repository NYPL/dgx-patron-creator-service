// The endpoints for NYPL's Simplified Card Creator API
module.exports = {
  ccBase: process.env.CARD_CREATOR_BASE_URL,
  ccCreatePatron: 'create_patron',
  patronSchemaName: 'Patron',
};
