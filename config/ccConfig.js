// The credentials for NYPL's Simplified Card Creator API
module.exports = {
  username: process.env.CARD_CREATOR_USERNAME,
  password: process.env.CARD_CREATOR_PASSWORD
};
